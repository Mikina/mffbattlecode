package teamZkazy;

import battlecode.common.Clock;
import battlecode.common.GameActionException;
import battlecode.common.GameActionExceptionType;
import battlecode.common.RobotController;

public strictfp class RobotPlayer {
	static RobotController rc;

	/**
	 * run() is the method that is called when a robot is instantiated in the
	 * Battlecode world. If this method returns, the robot dies!
	 **/
	@SuppressWarnings("unused")
	public static void run(RobotController rc) throws GameActionException {

		// This is the RobotController object. You use it to perform actions from this
		// robot,
		// and to get information on its current status.
		RobotPlayer.rc = rc;
		Robot robot;
		// Here, we've separated the controls into a different method for each
		// RobotType.
		// You can add the missing ones or rewrite this into your own control structure.
		switch (rc.getType()) {
		case ARCHON:
			robot = new Archon(rc);
			break;
		case GARDENER:
			robot = new Gardener(rc);
			break;
		case SOLDIER:
			robot = new Soldier(rc);
			break;
		case LUMBERJACK:
			robot = new Lumberjack(rc);
			break;
		case SCOUT:
			robot = new Scout(rc);
			break;
		case TANK:
			robot = new Tank(rc);
			break;
		default:
			throw new GameActionException(GameActionExceptionType.INTERNAL_ERROR, "Unknown robot type.");
		}

		// Pusti metodu ktera se provede po spusteni robota;
		robot.onStart();

		while (true) {
			

			// Try/catch blocks stop unhandled exceptions, which cause your robot to explode
			try {
				robot.managePoints();
				robot.updateBuyQueue();
				robot.doUpdate();
			} catch (Exception e) {
				System.out.println(robot.robotType.toString() + " Exception");
				e.printStackTrace();
			}

			// Clock.yield() makes the robot wait until the next turn, then it will perform
			// this loop again
			Clock.yield();
		}
	}
	
}
