package teamZkazy;

import battlecode.common.BulletInfo;
import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;
import battlecode.common.Team;
import battlecode.common.TreeInfo;

public class Soldier extends Robot {

	static final int RAPE_BROADCAST = 66;
	static final int RAPE_HEALTH = 68;
	static final int RAPE_ID = 69;
	static final int RAPE_ROUND = 600;
	
	static final float START_WANDER_DISTANCE = 10f;
	Team enemy;

	MapLocation goal;
	
	int deadArchons = 0;
	boolean archonsDead = false;

	public Soldier(RobotController rc) {
		super(rc, rc.getType());
		// TODO Auto-generated constructor stub
	}

	@Override
	public void doUpdate() throws GameActionException {

		MapLocation myLocation = rc.getLocation();
		BulletInfo[] bullets = getBulletsTowardsMe();
		RobotInfo[] robots = rc.senseNearbyRobots(robotType.sensorRadius, enemy);
		TreeInfo[] trees = rc.senseNearbyTrees();
		RobotInfo[] allies = rc.senseNearbyRobots(robotType.sensorRadius, enemy.opponent());
		MapLocation rape = readBroadcastLocation(RAPE_BROADCAST);
		
		if (rc.getRoundNum() > RAPE_ROUND) {
			if (!archonsDead)
				goal = enemyArchons[deadArchons];
		} else //Defend home strat;
		{
			goal = randomMoveLocation(allies,true);	
			
					if(!goal.isWithinDistance(rc.getInitialArchonLocations(rc.getTeam())[0], START_WANDER_DISTANCE))
						goal = rc.getInitialArchonLocations(rc.getTeam())[0];
										
					
					
		}


		shakeNearbyTrees(trees);
		updateArchonArray(robots);
		updateBroadcast(robots, rape);

		RobotInfo whoToKill = bestTarget(robots, trees, allies);
		if (whoToKill != null) {
			// if (myLocation.distanceTo(whoToKill.getLocation()) < 6f +
			// whoToKill.getRadius()) {
			fire(whoToKill);
			// }
			goal = whoToKill.getLocation().add(whoToKill.getLocation().directionTo(myLocation),
					whoToKill.getRadius() + 7f); // .add(whoToKill.getLocation().directionTo(myLocation), 6f +
			// whoToKill.getRadius());

			if (whoToKill.health > rc.readBroadcastFloat(RAPE_HEALTH)
					|| rc.readBroadcast(RAPE_ID) == whoToKill.getID()) {
				rc.broadcastFloat(RAPE_HEALTH, whoToKill.health);
				rc.broadcastInt(RAPE_ID, whoToKill.getID());
				broadcastLocation(whoToKill.getLocation(), RAPE_BROADCAST);
			}

		} else if (archonsDead) {
			if (rc.readBroadcastFloat(RAPE_ID) != -1)
				goal = readBroadcastLocation(RAPE_BROADCAST);
			else
				goal = myLocation.add(randomDirection(), robotType.strideRadius);
		}

		// Avoid Lumberjacks
		for (int i = 0; i < robots.length; i++) {
			if (robots[i].getType() == RobotType.LUMBERJACK
					&& myLocation.isWithinDistance(robots[i].getLocation(), robots[i].getRadius() + 3f)) {
				goal = myLocation.add(robots[i].getLocation().directionTo(myLocation), robotType.strideRadius);
				break;
			}

		}

		if (bullets.length == 0)
			findPathTowards(goal);
		else
			dodgeMoveTowards(goal, bullets);
	}

	public void fire(RobotInfo whoToKill) throws GameActionException {
		if (whereToFire == null || !whereToFire.isWithinDistance(whoToKill.getLocation(), whoToKill.getRadius() + 0.5f))
			whereToFire = whoToKill.getLocation();
		// TODO More firing modes and combat tactics.
		if (rc.canFireTriadShot() && (rc.getLocation().distanceTo(whoToKill.getLocation()) < 2f + whoToKill.getRadius()
				|| (whoToKill.getType() == RobotType.ARCHON)
						&& rc.getLocation().distanceTo(whoToKill.getLocation()) < 3f + whoToKill.getRadius()))
			rc.fireTriadShot(rc.getLocation().directionTo(whereToFire));
		else if (rc.canFireSingleShot())
			rc.fireSingleShot(rc.getLocation().directionTo(whereToFire));

	}

	@Override
	public void onStart() {

		System.out.println("I'm an soldier!");
		enemy = rc.getTeam().opponent();
		setUpArchonArray();

	}

	private MapLocation whereToFire;

	public RobotInfo bestTarget(RobotInfo[] nearbyEnemies, TreeInfo[] trees, RobotInfo[] allies)
			throws GameActionException {
		whereToFire = null;
		if (nearbyEnemies.length == 0)
			return null;

		int bestScore = Integer.MAX_VALUE;
		RobotInfo bestRobot = null;
		for (int i = 0; i < nearbyEnemies.length; i++) {
			MapLocation canHit = canHitTarget(rc.getLocation(), nearbyEnemies[i], trees, allies);
			if (canHit != null) {
				int score = getTargetTypeScore(nearbyEnemies[i], i);

				if (score < bestScore) {
					whereToFire = canHit;
					bestRobot = nearbyEnemies[i];
					bestScore = score;
				}
			}

		}

		return bestRobot;
	}



	public void updateArchonArray(RobotInfo[] robots) throws GameActionException {

		if (archonsDead)
			return;
		if (deadArchons < enemyArchons.length && rc.getLocation().distanceTo(enemyArchons[deadArchons]) < 4
				&& archonProbablyDead(robots))
			deadArchons++;

		if (deadArchons == enemyArchons.length)
			archonsDead = true;

	}

	public void updateBroadcast(RobotInfo[] robots, MapLocation broadcast) throws GameActionException {
		if (rc.getLocation().distanceTo(broadcast) < 4 && broadcastProbablyDead(robots)) {
			rc.broadcastFloat(RAPE_ID, -1);
		}
	}

	public boolean archonProbablyDead(RobotInfo[] robots) throws GameActionException {
		for (int i = 0; i < robots.length; i++)
			if (robots[i].getType() == RobotType.ARCHON)
				return false;
		return true;
	}

	public boolean broadcastProbablyDead(RobotInfo[] robots) throws GameActionException {
		return robots.length == 0;
	}



	protected int getTargetTypeScore(RobotInfo target, int startScore) {
		switch (target.getType()) {
		case ARCHON:
			startScore += 200;
			break;
		case GARDENER:
			startScore += 80;
			break;
		case LUMBERJACK:
			startScore += 120;
			break;
		case SCOUT:
			startScore += 160;
			break;
		case SOLDIER:
			startScore += 40;
			break;
		case TANK:
			startScore += 0;
			break;
		default:
			break;

		}

		return startScore;
	}

	public MapLocation canHitTarget(MapLocation shooter, RobotInfo target, TreeInfo[] trees, RobotInfo[] allies)
			throws GameActionException {
		Direction dir = target.getLocation().directionTo(shooter);
		MapLocation middle = target.location.add(dir, target.getType().bodyRadius);
		MapLocation top = target.location.add(dir.rotateLeftDegrees(90f), target.getType().bodyRadius);
		MapLocation bottom = target.location.add(dir.rotateRightDegrees(90f), target.getType().bodyRadius);

		boolean middleFlag = true;
		boolean topFlag = true;
		boolean bottomFlag = true;
		for (TreeInfo t : trees) {
			if (middleFlag) {
				if (distancePointToLine(t.location, shooter, middle) < t.radius) {
					middleFlag = false;
				}
			}
			if (topFlag) {
				if (distancePointToLine(t.location, shooter, top) < t.radius) {
					topFlag = false;
				}
			}
			if (bottomFlag) {
				if (distancePointToLine(t.location, shooter, bottom) < t.radius) {
					bottomFlag = false;
				}
			}
		}
		for (RobotInfo r : allies) {
			if (middleFlag) {
				if (distancePointToLine(r.location, shooter, middle) < r.getType().bodyRadius) {
					middleFlag = false;
				}
			}
			if (topFlag) {
				if (distancePointToLine(r.location, shooter, top) < r.getType().bodyRadius) {
					topFlag = false;
				}
			}
			if (bottomFlag) {
				if (distancePointToLine(r.location, shooter, bottom) < r.getType().bodyRadius) {
					bottomFlag = false;
				}
			}
		}
		if (middleFlag) {
			return middle;
		}
		if (topFlag) {
			return top;
		}
		if (bottomFlag) {
			return bottom;
		}
		return null;
	}

	// Borrowed code. Make our own.
	// https://brilliant.org/wiki/dot-product-distance-between-point-and-a-line/
	// ===========================================
	// ==========DISTANCE FROM POINT TO LINE======
	// ===========================================
	// this method actually works, as opposed to the one in RobotBase
	private double sqr(double x) {
		return x * x;
	}

	private double dist2(MapLocation v, MapLocation w) {
		return (sqr(v.x - w.x) + sqr(v.y - w.y));
	}

	private double distToSegmentSquared(MapLocation p, MapLocation v, MapLocation w) {
		double l2 = dist2(v, w);
		if (l2 == 0) {
			return dist2(p, v);
		}
		double t = ((p.x - v.x) * (w.x - v.x) + (p.y - v.y) * (w.y - v.y)) / l2;
		t = Math.max(0, Math.min(1, t));
		MapLocation newLoc = new MapLocation((float) (v.x + t * (w.x - v.x)), (float) (v.y + t * (w.y - v.y)));
		return dist2(p, newLoc);
	}

	private double distancePointToLine(MapLocation p, MapLocation v, MapLocation w) {
		return Math.sqrt(distToSegmentSquared(p, v, w));
	}
	// ===============================================
	// ==========END DISTANCE FROM POINT TO LINE======
	// ===============================================

}
