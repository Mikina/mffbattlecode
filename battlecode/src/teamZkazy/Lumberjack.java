package teamZkazy;

import battlecode.common.BulletInfo;
import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.GameConstants;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;
import battlecode.common.Team;
import battlecode.common.TreeInfo;

public class Lumberjack extends Robot {

	Team enemy;
	
	public Lumberjack(RobotController rc) {
		super(rc, RobotType.LUMBERJACK);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void doUpdate() throws GameActionException{

		


				// See if there are any enemy robots within striking range (distance 1 from
				// lumberjack's radius)
				RobotInfo[] robots = rc.senseNearbyRobots(
						RobotType.LUMBERJACK.bodyRadius + GameConstants.LUMBERJACK_STRIKE_RADIUS - 0.1f, enemy);
				RobotInfo[] allies = rc.senseNearbyRobots(
						RobotType.LUMBERJACK.bodyRadius + GameConstants.LUMBERJACK_STRIKE_RADIUS - 0.1f, enemy);
				TreeInfo[] alliedTrees = rc.senseNearbyTrees(
						RobotType.LUMBERJACK.bodyRadius + GameConstants.LUMBERJACK_STRIKE_RADIUS - 0.1f, enemy);
				TreeInfo[] trees = rc
						.senseNearbyTrees();
				shakeNearbyTrees(trees);
						
						RobotInfo[] allAllies = rc.senseNearbyRobots(robotType.sensorRadius, rc.getTeam());
				

				
				MapLocation goal = new MapLocation(0,0);
				if(trees.length	> 0)
					goal = trees[0].getLocation();
				else
					goal = randomMoveLocation(allAllies,true);

				boolean stop = false;

				for (TreeInfo tree: trees){
					if (rc.canChop(tree.ID) && tree.team != myTeam) {
						rc.chop(tree.ID);
						return;
					}
				}
				if (!stop && robots.length > allies.length + alliedTrees.length && !rc.hasAttacked()) {
					// Use strike() to hit all nearby robots!
					rc.strike();
				}
					
					BulletInfo[] bullets = getBulletsTowardsMe();
					if(bullets.length > 0)
						dodgeMoveTowards(goal, bullets);
					else
						findPathTowards(goal);
				
	}



	@Override
	public void onStart() {

		System.out.println("I'm a lumberjack!");
		enemy = rc.getTeam().opponent();
		
	}

}
