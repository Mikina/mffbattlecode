package teamZkazy;

import battlecode.common.*;

public class Archon extends Robot {
	
	static boolean main = false;

	public Archon(RobotController rc) {
		super(rc, RobotType.ARCHON);
	}

	protected void setArchonEdgeMapLocation() {
		edgeMapLocationTop =- GameConstants.MAP_MAX_HEIGHT;
		edgeMapLocationRight =- GameConstants.MAP_MAX_WIDTH;

		for (MapLocation myArchonLoc: myArchons) {
			edgeMapLocationTop = Math.min(myArchonLoc.x, edgeMapLocationTop);
			edgeMapLocationRight = Math.min(myArchonLoc.y, edgeMapLocationRight);
			edgeMapLocationBottom = Math.max(myArchonLoc.x - GameConstants.MAP_MAX_WIDTH, edgeMapLocationBottom);
			edgeMapLocationLeft = Math.max(myArchonLoc.y - GameConstants.MAP_MAX_HEIGHT, edgeMapLocationLeft);
		}

		for (MapLocation enemyArchonLoc: enemyArchons) {
			edgeMapLocationTop = Math.min(enemyArchonLoc.x, edgeMapLocationTop);
			edgeMapLocationRight = Math.min(enemyArchonLoc.y, edgeMapLocationRight);
			edgeMapLocationBottom = Math.max(enemyArchonLoc.x - GameConstants.MAP_MAX_WIDTH, edgeMapLocationBottom);
			edgeMapLocationLeft = Math.max(enemyArchonLoc.y - GameConstants.MAP_MAX_HEIGHT, edgeMapLocationLeft);
		}

		edgeMapLocationTop =+ GameConstants.MAP_MAX_HEIGHT;
		edgeMapLocationRight =+ GameConstants.MAP_MAX_WIDTH;
	}
	
	@Override
	public void doUpdate() throws GameActionException {

		Direction dir = randomDirection();
		
		//Build units
		RobotType topQueue = checkTopOfQueue();
		if (topQueue != null && rc.hasRobotBuildRequirements(topQueue) && rc.canBuildRobot(topQueue, dir) && rc.canHireGardener(dir)) {
			RobotType toBuild = getFromQueue();
			rc.buildRobot(toBuild, dir);
		}
				
		// Move randomly
		tryMove(randomDirection());

		//Check gardener count, then reset it
		if (rc.getRobotCount() < 4 && rc.canHireGardener(dir)) {
			rc.hireGardener(dir);
		}
	
	}

	@Override
	public void onStart() {
		try {
			setArchonEdgeMapLocation();

			if (checkStrategy() == STRATEGY_NONE) {
				main = true;
				addToBuildQueue(RobotType.GARDENER);
				addToBuildQueue(RobotType.SCOUT);
				addToBuildQueue(RobotType.SCOUT);
				
				//If one archon and close, rush
				if (rc.getInitialArchonLocations(rc.getTeam().opponent()).length == 1 && rc.getLocation().distanceTo(rc.getInitialArchonLocations(rc.getTeam().opponent())[0]) < 50) {
					setStrategy(STRATEGY_RUSH);
					addToBuildQueue(RobotType.SOLDIER);
					addToBuildQueue(RobotType.SOLDIER);
					addToBuildQueue(RobotType.SOLDIER);
					addToBuildQueue(RobotType.LUMBERJACK);
					addToBuildQueue(RobotType.GARDENER);
					addToBuildQueue(RobotType.SOLDIER);
					addToBuildQueue(RobotType.GARDENER);
				} else if (rc.senseNearbyTrees().length > 4) {
					//Between trees
					setStrategy(STRATEGY_TREES);
					addToBuildQueue(RobotType.LUMBERJACK);
					addToBuildQueue(RobotType.LUMBERJACK);
					addToBuildQueue(RobotType.GARDENER);
					addToBuildQueue(RobotType.GARDENER);
					addToBuildQueue(RobotType.LUMBERJACK);
					addToBuildQueue(RobotType.SOLDIER);
					addToBuildQueue(RobotType.SOLDIER);					
				} else if (rc.senseNearbyTrees().length == 0) {
					//Lots of space, economy strategy
					setStrategy(STRATEGY_ECONOMY);
					addToBuildQueue(RobotType.GARDENER);
					addToBuildQueue(RobotType.GARDENER);
					addToBuildQueue(RobotType.GARDENER);
					addToBuildQueue(RobotType.GARDENER);
					addToBuildQueue(RobotType.GARDENER);
					addToBuildQueue(RobotType.SOLDIER);
					addToBuildQueue(RobotType.SOLDIER);					
					addToBuildQueue(RobotType.SCOUT);
					addToBuildQueue(RobotType.SCOUT);
				} else {
					setStrategy(STRATEGY_NORMAL);
					addToBuildQueue(RobotType.GARDENER);
					addToBuildQueue(RobotType.GARDENER);
					addToBuildQueue(RobotType.SOLDIER);					
					addToBuildQueue(RobotType.LUMBERJACK);
					addToBuildQueue(RobotType.SCOUT);
					addToBuildQueue(RobotType.GARDENER);
					addToBuildQueue(RobotType.SOLDIER);					
					addToBuildQueue(RobotType.SCOUT);
				}
				printBuildQueue();
			}
		} catch (GameActionException e1) {
			e1.printStackTrace();
		}
	}

}
