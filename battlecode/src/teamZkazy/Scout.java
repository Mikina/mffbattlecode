package teamZkazy;

import battlecode.common.*;

public class Scout extends Robot {

	public Scout(RobotController rc) {
		super(rc,RobotType.SCOUT);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void doUpdate() throws GameActionException {

		// TODO Auto-generated method stub
		MapLocation myLocation = rc.getLocation();
		MapLocation[] possibleMoves = new MapLocation[100];
		TreeInfo[] trees = rc.senseNearbyTrees();
		BulletInfo[] bullets = getBulletsTowardsMe();
		RobotInfo[] enemyRobots = rc.senseNearbyRobots(-1, enemyTeam);
		shakeNearbyTrees(trees);
		MapLocation goal = rc.getLocation().add(randomDirection(),robotType.strideRadius);



		int numOfMoves = 0;

		if (enemyRobots.length > 0) {
			for (RobotInfo enemyRobot: enemyRobots) {
				Direction dir = myLocation.directionTo(enemyRobot.location);
				possibleMoves[numOfMoves] = myLocation.add(dir.opposite(), robotType.strideRadius);
				numOfMoves++;
			}
		}

		boolean found = false;
		if(trees.length > 0) {
			for (TreeInfo tree: trees) {
				possibleMoves[numOfMoves] = tree.getLocation();
				numOfMoves++;
				if (tree.getContainedBullets() > 0){
					goal = tree.getLocation();
					found = true;
					break;
				}
			}
		}
		
		if (numOfMoves > 0 && !found) {
			//int n = rand.nextInt(numOfMoves);
			int rand =  (int) Math.random()*(numOfMoves-1);
			goal = possibleMoves[rand];
		}



		dodgeMoveTowards(goal, bullets);

		//determineMapSize();

	}

	@Override
	public void onStart() {
			
		
	}

}
