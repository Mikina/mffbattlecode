package teamZkazy;

import battlecode.common.BulletInfo;
import battlecode.common.Clock;
import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;
import battlecode.common.TreeInfo;

public class Gardener extends Robot {

	public static int GARDENER_RADIUS = 2;
	static Direction treeDirections[];
	static Direction buildDirection;
	static boolean wandering = true;
	static boolean tankGardener = false;
	static int wanderSteps = 0;
	
	public Gardener(RobotController rc) {
		super(rc, RobotType.GARDENER);
	}

	@Override
	public void doUpdate() throws GameActionException {
			//Build units
			//Soldier test code.;
			//topQueue = robotType.SOLDIER;
			gardenerReportingIn();
		
			TreeInfo[] trees = rc.senseNearbyTrees();
			shakeNearbyTrees(trees);

			//Find location for tree flower
			while (wandering) {
				RobotType topQueue = checkTopOfQueue();
				if (topQueue != null) {
					Direction[] dirs = this.circleDirections(buildDirection);
					for	(Direction dir : dirs) {
						if (rc.canBuildRobot(topQueue, dir)) {
							RobotType toBuild = getFromQueue();
							rc.buildRobot(toBuild, dir);
							break;
						}						
					}				
				}

				
				if (!rc.isCircleOccupiedExceptByThisRobot(rc.getLocation(), GARDENER_RADIUS + 1)) {
					wandering = false;
					removeWanderingGardener();
					break;
				}

				RobotInfo[] allies = rc.senseNearbyRobots(robotType.sensorRadius, rc.getTeam());
				
				MapLocation goal = null;
				if (wanderSteps < 1000) {
					MapLocation randomGoal = randomMoveLocation(allies,true);
					randomGoal = randomGoal.add(rc.getLocation().directionTo(randomGoal),robotType.strideRadius*2f);
					goal = randomGoal;				
				} else {
					goal = rc.getInitialArchonLocations(enemyTeam)[0];
				}

				if (rc.getHealth() < 7) {
					removeWanderingGardener();
				}
					BulletInfo[] bullets = getBulletsTowardsMe();
					if(bullets.length > 0)
						dodgeMoveTowards(goal, bullets);
					else
						findPathTowards(goal);
				wanderSteps++;
				Clock.yield();
			}
			
			RobotType topQueue = checkTopOfQueue();
			if (topQueue != null) {
				Direction[] dirs = this.circleDirections(buildDirection);
				for	(Direction dir : dirs) {
					if (rc.canBuildRobot(topQueue, dir)) {
						RobotType toBuild = getFromQueue();
						rc.buildRobot(toBuild, dir);
						break;
					}						
				}				
			}
			
			//Build trees
			for (int i = 0; i < (tankGardener ? 4 : 5); i++) {
				MapLocation plantTreeHere = rc.getLocation().add(treeDirections[i], GARDENER_RADIUS);
				if (!rc.isLocationOccupiedByTree(plantTreeHere) && rc.canPlantTree(treeDirections[i])) {
					rc.plantTree(treeDirections[i]);
					break;
				}
			}
			
			//Water trees
			TreeInfo treeToWater = null;
			for (TreeInfo tree : rc.senseNearbyTrees(GARDENER_RADIUS, rc.getTeam())) {
				if (treeToWater == null) {
					treeToWater = tree;
					continue;
				} else if (tree.health < treeToWater.health) {
					treeToWater = tree;
				}
			}
			
			if (treeToWater != null && rc.canWater(treeToWater.getLocation())) {
				rc.water(treeToWater.getLocation());
			}
	}

	@Override
	public void onStart() {
		try {
			addWanderingGardener();
		} catch (GameActionException e) {
		}
		//is this a tank gardener? Choose at random
		if (Math.random() > 0.5) {
			tankGardener = true;
		}
		
		treeDirections = new Direction[tankGardener ? 4 : 5];
		buildDirection = rc.getLocation().directionTo(rc.getInitialArchonLocations(rc.getTeam().opponent())[0]);
		Direction dir = buildDirection.rotateLeftDegrees(60);
		for (int i = 0; i < (tankGardener ? 4 : 5); i++) {
			treeDirections[i] = dir;
			dir = dir.rotateLeftDegrees(60);
		}
	}

}
