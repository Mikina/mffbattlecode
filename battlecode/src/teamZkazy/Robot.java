package teamZkazy;

import battlecode.common.*;
import java.util.ArrayList;
import java.util.List;

import battlecode.common.BulletInfo;
import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;
import battlecode.common.TreeInfo;

public abstract class Robot {

	protected static RobotController rc;
	
	public final RobotType robotType;

	public final int MAP_EDGE_BROADCAST_OFFSET = 10;

	public MapLocation[] enemyArchons;

	public Team myTeam;

	public Team enemyTeam;

	protected MapLocation[] myArchons;

	public int mapEdgesDetermined = 0;

	public static float edgeMapLocationTop, edgeMapLocationRight, edgeMapLocationBottom, edgeMapLocationLeft;





	public Robot(RobotController rc, RobotType robotType) {
		this.robotType = robotType;
		Robot.rc = rc;

		MapLocation spawnLoc = rc.getLocation();
		this.myTeam = rc.getTeam();
		this.enemyTeam = myTeam.opponent();
		this.myArchons = rc.getInitialArchonLocations(myTeam);
		setUpArchonArray();

		edgeMapLocationTop = Math.min(spawnLoc.x, 500f)+ GameConstants.MAP_MAX_HEIGHT;
		edgeMapLocationRight = Math.min(spawnLoc.y, 500f)+ GameConstants.MAP_MAX_HEIGHT;

		edgeMapLocationBottom = Math.max(spawnLoc.x - GameConstants.MAP_MAX_HEIGHT, 0f);
		edgeMapLocationLeft = Math.max(spawnLoc.y - GameConstants.MAP_MAX_WIDTH, 0f);


	}

	public abstract void doUpdate() throws GameActionException;

	public abstract void onStart() throws GameActionException;

	public static int STRATEGY_CHANNEL = 297;
	public static int STRATEGY_NONE = 0;
	public static int STRATEGY_RUSH = 1;
	public static int STRATEGY_NORMAL = 2;
	public static int STRATEGY_ECONOMY = 3;
	public static int STRATEGY_TREES = 4;
	
	public static int BQ_POINTER_CHANNEL = 298;
	public static int BQ_LENGTH_CHANNEL = 299;
	public static int BQ_LOWEST = 300;
	public static int BQ_HIGHEST = 400;
	public static int BQ_MAX_QUEUED = 100;
	
	public static int WANDERING_GARDENER_CHANNEL = 296;
	public static int GARDENER_COUNT_CHANNEL = 295;
	
	//Kolik mista maji nechat gardenerum pri random chozeni kolem soldieri.
	public final float GARDENER_AVOID_RADIUS = 2f;
	
	protected void gardenerReportingIn() throws GameActionException
	{
		int cnt = rc.readBroadcast(GARDENER_COUNT_CHANNEL);
		rc.broadcast(GARDENER_COUNT_CHANNEL, cnt + 1);
	}
	
	protected void gardenerCountReset() throws GameActionException
	{
		rc.broadcast(GARDENER_COUNT_CHANNEL, 0);
	}

	protected int getGardenerCount() throws GameActionException
	{
		return rc.readBroadcast(GARDENER_COUNT_CHANNEL);
	}
	
	protected void addWanderingGardener() throws GameActionException
	{
		int cnt = rc.readBroadcast(WANDERING_GARDENER_CHANNEL);
		rc.broadcast(WANDERING_GARDENER_CHANNEL, cnt + 1);
	}
	
	protected void removeWanderingGardener() throws GameActionException
	{
		int cnt = rc.readBroadcast(WANDERING_GARDENER_CHANNEL);
		rc.broadcast(WANDERING_GARDENER_CHANNEL, cnt - 1 < 0 ? 0 : cnt - 1);		
	}
	
	protected int getWanderingGardenerCount() throws GameActionException
	{
		return rc.readBroadcast(WANDERING_GARDENER_CHANNEL);
	}
	
	protected int checkStrategy() throws GameActionException
	{
		int strategy = rc.readBroadcast(STRATEGY_CHANNEL);
		return (strategy >= 1 && strategy <= 4) ? strategy : STRATEGY_NONE;
	}
	
	protected void setStrategy(int strategy) throws GameActionException
	{
		if (strategy >= 1 && strategy <= 4) {
			rc.broadcast(STRATEGY_CHANNEL, strategy);
		}
	}
	
	public void shakeNearbyTrees(TreeInfo[] nearbyTrees) throws GameActionException {
		for (int i = 0; i < nearbyTrees.length; i++) {
			if (nearbyTrees[i].getContainedBullets() > 0 && rc.canShake(nearbyTrees[i].getID())) {
				rc.shake(nearbyTrees[i].getID());
				break;
			}
		}

	}
	
	public void updateBuyQueue() throws GameActionException {
		int length = getQueueLength();
		if (length > 0) return;

		System.out.println("ADDING TO QUEUE");

        int strategy = checkStrategy();
		double random = Math.random();
		switch(strategy) {
		//Rush
		case 1:
			if (random < 0.1) {
				addToBuildQueue(RobotType.GARDENER);
			} else if (random < 0.6) {
				addToBuildQueue(RobotType.SOLDIER);
			} else if (random < 0.8) {
				addToBuildQueue(RobotType.TANK);
			} else {
				addToBuildQueue(RobotType.LUMBERJACK);
			}
			break;
		//Economy
		case 2:
			if (random < 0.2) {
				addToBuildQueue(RobotType.GARDENER);
			} else if (random < 0.7) {
				addToBuildQueue(RobotType.SOLDIER);
			} else if (random < 0.95) {
				addToBuildQueue(RobotType.LUMBERJACK);
			} else {
				addToBuildQueue(RobotType.SCOUT);
			}
			break;
		//Normal
		case 3:
			//everything with same chance
			if (random < 0.2) {
				addToBuildQueue(RobotType.GARDENER);
			} else if (random < 0.5) {
				addToBuildQueue(RobotType.SOLDIER);
			} else if (random < 0.7) {
				addToBuildQueue(RobotType.TANK);
			} else if (random < 0.95) {
				addToBuildQueue(RobotType.LUMBERJACK);
			} else {
				addToBuildQueue(RobotType.SCOUT);
			}
			break;
		//Trees
		case 4:
			if (random < 0.2) {
				addToBuildQueue(RobotType.GARDENER);
			} else if (random < 0.3) {
				addToBuildQueue(RobotType.SOLDIER);
			} else if (random < 0.5) {
				addToBuildQueue(RobotType.TANK);
			} else if (random < 0.95) {
				addToBuildQueue(RobotType.LUMBERJACK);
			} else {
				addToBuildQueue(RobotType.SCOUT);
			}
			break;
		}
	}

	public void managePoints() throws GameActionException {
		//If enemy is buying, buy too
		if (rc.getOpponentVictoryPoints() > (rc.getTeamVictoryPoints() + 10)) {
			int difference = rc.getOpponentVictoryPoints() - rc.getTeamVictoryPoints();
			float cost = difference * rc.getVictoryPointCost();
			if (rc.getTeamBullets() > cost) {
				rc.donate(cost);
			}
		}

		//Buy VP to win, if enough bullets
		if (rc.getTeamBullets()/rc.getVictoryPointCost() >= 1000) {
			rc.donate(rc.getTeamBullets());
		}
	}

	protected int getQueueLength() throws GameActionException {
		return rc.readBroadcast(BQ_LENGTH_CHANNEL);
	}

	protected void addToBuildQueue(RobotType robotToBuild) throws GameActionException {
		int robotInt = -1;
		switch (robotToBuild) {
		case GARDENER:
			robotInt = 0;			
			if (getWanderingGardenerCount() >= 3) {
				return;
			}			
			break;
		case SOLDIER:
			robotInt = 1;
			break;
		case TANK:
			robotInt = 2;
			break;
		case LUMBERJACK:
			robotInt = 3;
			break;
		case SCOUT:
			robotInt = 4;
			break;
		}
		if (robotInt == -1) {
			return;
		}

		int currentLength = rc.readBroadcast(BQ_LENGTH_CHANNEL);
		int currentPointer = rc.readBroadcast(BQ_POINTER_CHANNEL);
		if (currentLength + 1 > BQ_MAX_QUEUED) {
			return;
		}
		rc.broadcast(((currentPointer + currentLength) % BQ_MAX_QUEUED) + BQ_LOWEST, robotInt);
		rc.broadcast(BQ_LENGTH_CHANNEL, currentLength + 1);
		System.out.println("Adding to queue / length: " + (currentLength + 1) + " current pointer " + currentPointer);
	}

	protected RobotType checkTopOfQueue() throws GameActionException {
		int currentLength = rc.readBroadcast(BQ_LENGTH_CHANNEL);
		if (currentLength <= 0) {
			System.out.println("Nothing on top");
			return null;
		}

		int currentPointer = rc.readBroadcast(BQ_POINTER_CHANNEL);
		int topNumber = rc.readBroadcast((currentPointer % BQ_MAX_QUEUED) + BQ_LOWEST);
		System.out.println("Top Number " + topNumber + " taken from " + ((currentPointer % BQ_MAX_QUEUED) + BQ_LOWEST));
		switch (topNumber) {
		case 0:
			return RobotType.GARDENER;
		case 1:
			return RobotType.SOLDIER;
		case 2:
			return RobotType.TANK;
		case 3:
			return RobotType.LUMBERJACK;
		case 4:
			return RobotType.SCOUT;
		default:
			return null;
		}
	}
	
	protected void printBuildQueue() throws GameActionException {
		int currentLength = rc.readBroadcast(BQ_LENGTH_CHANNEL);
		int currentPointer = rc.readBroadcast(BQ_POINTER_CHANNEL);

		for (int i = 0; i < currentLength; i++) {
			
			int topNumber = rc.readBroadcast((currentPointer % BQ_MAX_QUEUED + i) + BQ_LOWEST);
			System.out.println(
					"Returning " + topNumber + " pointer " + ((currentPointer % BQ_MAX_QUEUED + i) + BQ_LOWEST) + " length " + (currentLength));
			

		}
	}

	protected RobotType getFromQueue() throws GameActionException {
		int currentLength = rc.readBroadcast(BQ_LENGTH_CHANNEL);
		int currentPointer = rc.readBroadcast(BQ_POINTER_CHANNEL);
		if (currentLength <= 0) {
			return null;
		}

		int topNumber = rc.readBroadcast((currentPointer % BQ_MAX_QUEUED) + BQ_LOWEST);
		rc.broadcast(BQ_LENGTH_CHANNEL, currentLength - 1);
		rc.broadcast(BQ_POINTER_CHANNEL, currentPointer + 1);

		System.out.println(
				"Returning " + topNumber + " pointer " + (currentPointer + 1) + " length " + (currentLength - 1));

		switch (topNumber) {
		case 0:
			return RobotType.GARDENER;
		case 1:
			return RobotType.SOLDIER;
		case 2:
			return RobotType.TANK;
		case 3:
			return RobotType.LUMBERJACK;
		case 4:
			return RobotType.SCOUT;
		default:
			return null;
		}
	}

	/**
	 * Returns a random Direction
	 * 
	 * @return a random Direction
	 */
	protected Direction randomDirection() {
		return new Direction((float) Math.random() * 2 * (float) Math.PI);
	}

	protected MapLocation randomMoveLocation(RobotInfo[] allies, boolean avoidGardeners) throws GameActionException {

		Direction[] dirs = circleDirections(randomDirection());
		MapLocation goal = rc.getLocation().add(dirs[0], robotType.strideRadius);
		if (avoidGardeners) {
			for (int i = 0; i < dirs.length; i++) {
				goal = rc.getLocation().add(dirs[i], robotType.strideRadius);
				Direction avoidDir = avoidGardeners(allies, goal);
				if (avoidDir == null)
					return goal;
				else
					goal = goal.add(avoidDir, robotType.strideRadius);
			}
		}
		return goal;

	}

	protected Direction avoidGardeners(RobotInfo[] allies, MapLocation goal) {
		MapLocation avoidDir = new MapLocation(0, 0);
		for (int i = 0; i < allies.length; i++) {
			if (allies[i].getType() == RobotType.GARDENER
					&& goal.isWithinDistance(allies[i].getLocation(), RobotType.GARDENER.bodyRadius + GARDENER_AVOID_RADIUS)) {
				Direction avoid = allies[i].getLocation().directionTo(goal);
				avoidDir.add(avoid);

			}
		}
		if (avoidDir.x != 0 || avoidDir.y != 0)
			return new MapLocation(0, 0).directionTo(avoidDir);

		return null;
	}

	/**
	 * Attempts to move in a given direction, while avoiding small obstacles
	 * directly in the path.
	 *
	 * @param dir The intended direction of movement
	 * @return true if a move was performed
	 * @throws GameActionException
	 */
	protected boolean tryMove(Direction dir) throws GameActionException {
		return tryMove(dir, 20, 3);
	}

	/**
	 * Attempts to move in a given direction, while avoiding small obstacles
	 * direction in the path.
	 *
	 * @param dir           The intended direction of movement
	 * @param degreeOffset  Spacing between checked directions (degrees)
	 * @param checksPerSide Number of extra directions checked on each side, if
	 *                      intended direction was unavailable
	 * @return true if a move was performed
	 * @throws GameActionException
	 */
	protected boolean tryMove(Direction dir, float degreeOffset, int checksPerSide) throws GameActionException {

		// First, try intended direction
		if (rc.canMove(dir)) {
			rc.move(dir);
			return true;
		}

		// Now try a bunch of similar angles
		boolean moved = false;
		int currentCheck = 1;

		while (currentCheck <= checksPerSide) {
			// Try the offset of the left side
			if (rc.canMove(dir.rotateLeftDegrees(degreeOffset * currentCheck))) {
				rc.move(dir.rotateLeftDegrees(degreeOffset * currentCheck));
				return true;
			}
			// Try the offset on the right side
			if (rc.canMove(dir.rotateRightDegrees(degreeOffset * currentCheck))) {
				rc.move(dir.rotateRightDegrees(degreeOffset * currentCheck));
				return true;
			}
			// No move performed, try slightly further
			currentCheck++;
		}

		// A move never happened, so return false.
		return false;
	}

	/**
	 * A slightly more complicated example function, this returns true if the given
	 * bullet is on a collision course with the current robot. Doesn't take into
	 * account objects between the bullet and this robot.
	 *
	 * @param bullet The bullet in question
	 * @return True if the line of the bullet's path intersects with this robot's
	 *         current position.
	 */
	protected boolean bulletWillCollide(BulletInfo bullet, MapLocation myLocation) {

		// Get relevant bullet information
		Direction propagationDirection = bullet.dir;
		MapLocation bulletLocation = bullet.location;

		// Calculate bullet relations to this robot
		Direction directionToRobot = bulletLocation.directionTo(myLocation);
		float distToRobot = bulletLocation.distanceTo(myLocation);
		float theta = propagationDirection.radiansBetween(directionToRobot);

		// distToRobot is our hypotenuse, theta is our angle, and we want to know this
		// length of the opposite leg.
		// This is the distance of a line that goes from myLocation and intersects
		// perpendicularly with propagationDirection.
		// This corresponds to the smallest radius circle centered at our location that
		// would intersect with the
		// line that is the path of the bullet.
		float perpendicularDist = (float) Math.abs(distToRobot * Math.sin(theta)); // soh cah toa :)

		return (perpendicularDist <= rc.getType().bodyRadius);
	}

	protected boolean directMoveTowards(MapLocation goal) throws GameActionException {
		Direction directionTo = rc.getLocation().directionTo(goal);
		if (rc.canMove(directionTo)) {
			rc.move(directionTo);
			return true;
		} else

		{
			return tryMove(directionTo);
		}
	}

	protected void determineMapSize() throws GameActionException {
		// Abort if all map edges have already been determined
		if (mapEdgesDetermined == 0xF) return;

		int globalMapEdgesDetermined = rc.readBroadcast(MAP_EDGE_BROADCAST_OFFSET);
		if (globalMapEdgesDetermined != mapEdgesDetermined) {
			mapEdgesDetermined = globalMapEdgesDetermined;
			edgeMapLocationTop = rc.readBroadcastFloat(MAP_EDGE_BROADCAST_OFFSET + (0 + 1));
			edgeMapLocationRight = rc.readBroadcastFloat(MAP_EDGE_BROADCAST_OFFSET + (1 + 1));
			edgeMapLocationBottom = rc.readBroadcastFloat(MAP_EDGE_BROADCAST_OFFSET + (2 + 1));
			edgeMapLocationLeft = rc.readBroadcastFloat(MAP_EDGE_BROADCAST_OFFSET + (3 + 1));
			return;
		}

		int tmpDetermined = mapEdgesDetermined;
		for (int i = 0; i < 4; i++) {
			if ((mapEdgesDetermined & (1 << i)) == 0) {
				float angle = i * (float)Math.PI / 2f;
				if (!rc.onTheMap(rc.getLocation().add(angle, robotType.sensorRadius ))) {
					// Found map edge
					float mn = 0f;
					float mx = robotType.sensorRadius;
					while (mx - mn > 0.002f) {
						float mid = (mn + mx) / 2;
						if (rc.onTheMap(rc.getLocation().add(angle, mid))) {
							mn = mid;
						} else {
							mx = mid;
						}
					}

					MapLocation mapEdge = rc.getLocation().add(angle, mn);
					float result = i % 2 == 0 ? mapEdge.x : mapEdge.y;
					rc.broadcastFloat(MAP_EDGE_BROADCAST_OFFSET + i + 1, result);
					// This robot will pick up the change for real the next time determineMapSize is called
					tmpDetermined |= (1 << i);
					rc.broadcast(MAP_EDGE_BROADCAST_OFFSET, tmpDetermined);

					// We also know that the other edge of the map is no further than MAX_WIDTH away from this edge
					float otherEdge = i < 2 ? result - GameConstants.MAP_MAX_WIDTH : result + GameConstants.MAP_MAX_WIDTH;
					int otherEdgeIndex = MAP_EDGE_BROADCAST_OFFSET + ((i + 2) % 4) + 1;

					// Make sure we don't overwrite the edge with something worse
					float prevValueForOtherEdge = rc.readBroadcastFloat(otherEdgeIndex);
					if (i < 2) otherEdge = Math.max(otherEdge, prevValueForOtherEdge);
					else otherEdge = Math.min(otherEdge, prevValueForOtherEdge);

					rc.broadcastFloat(otherEdgeIndex, otherEdge);

					rc.setIndicatorLine(mapEdge.add(angle + (float)Math.PI * 0.5f, 50), mapEdge.add(angle - (float)Math.PI * 0.5f, 50), 255, 255, 255);
				}
			}
		}
	}




	protected void broadcastLocation(MapLocation location, int index) throws GameActionException {
		rc.broadcastFloat(index, location.x);
		rc.broadcastFloat(index + 1, location.y);
	}

	protected MapLocation readBroadcastLocation(int index) throws GameActionException {
		MapLocation loc = new MapLocation(rc.readBroadcastFloat(index), rc.readBroadcastFloat(index + 1));

		return loc;

	}

	MapLocation prevPrevLocation;
	MapLocation prevLocation;

	protected boolean findPathTowards(MapLocation goal) throws GameActionException {
		Direction[] dirs = circleDirections(rc.getLocation().directionTo(goal));

		float stride = robotType.strideRadius;
		MapLocation loc = rc.getLocation();
		Direction goalDir = loc.directionTo(goal);

		Direction bestDir = dirs[0];
		float bestScore = Float.MAX_VALUE;

		for (int i = 0; i < dirs.length; i++) {
			if (rc.canMove(dirs[i], stride)) {
				MapLocation desiredLoc = loc.add(dirs[i], stride);
				float currentScore = desiredLoc.distanceTo(goal);

				if (prevLocation != null)
					currentScore -= desiredLoc.distanceTo(prevLocation) * 1.5;

				if (prevPrevLocation != null)
					currentScore -= desiredLoc.distanceTo(prevPrevLocation) * 0.5;

				if (currentScore < bestScore) {
					bestScore = currentScore;
					bestDir = dirs[i];
				}

			}
		}

		prevPrevLocation = prevLocation;
		prevLocation = loc;

		if (rc.canMove(bestDir)) {
			rc.move(bestDir);
			return true;
		}

		return false;
	}

	protected Direction[] circleDirections(Direction startDir) throws GameActionException {
		Direction[] dirs = new Direction[12];
		dirs[0] = startDir;
		for (int j = 1; j < dirs.length; j++) {
			dirs[j] = dirs[j - 1].rotateLeftDegrees(30);
		}
		return dirs;
	}





	protected void setUpArchonArray() {
		enemyArchons = rc.getInitialArchonLocations(rc.getTeam().opponent());
		for (int i = 0; i < enemyArchons.length; i++) {
			for (int j = 0; j < enemyArchons.length; j++) {
				if (rc.getLocation().distanceTo(enemyArchons[i]) < rc.getLocation().distanceTo(enemyArchons[j])) {
					MapLocation temp = enemyArchons[i];
					enemyArchons[i] = enemyArchons[j];
					enemyArchons[j] = temp;
				}
			}
		}
	}

	protected boolean dodgeMoveTowards(MapLocation goal, BulletInfo[] bullets) throws GameActionException {
		Direction[] dirs = circleDirections(rc.getLocation().directionTo(goal));

		float stride = robotType.strideRadius;
		MapLocation loc = rc.getLocation();
		Direction goalDir = loc.directionTo(goal);

		Direction bestDir = dirs[0];
		float bestScore = Float.MAX_VALUE;

		for (int i = 0; i < dirs.length; i++) {
			if (rc.canMove(dirs[i], stride)) {
				MapLocation desiredLoc = loc.add(dirs[i], stride);
				float currentScore = desiredLoc.distanceTo(goal);

				for (int j = 0; j < bullets.length; j++) {
					if (bulletWillCollide(bullets[j], desiredLoc))
						currentScore += 1000;
				}

				if (currentScore < bestScore) {
					bestScore = currentScore;
					bestDir = dirs[i];
				}

			}
		}

		if (rc.canMove(bestDir)) {
			rc.move(bestDir);
			return true;
		}

		return false;
	}
	
	public BulletInfo[] getBulletsTowardsMe() throws GameActionException {
		BulletInfo[] nearby = rc.senseNearbyBullets(5f);
		List<BulletInfo> dangerous = new ArrayList<BulletInfo>();
		for (int i = 0; i < nearby.length; i++) {
			if (!bulletHeadingAway(nearby[i]))
				dangerous.add(nearby[i]);
		}

		BulletInfo[] toReturn = new BulletInfo[dangerous.size()];
		return dangerous.toArray(toReturn);

	}

	public boolean bulletHeadingAway(BulletInfo bullet) {
		return rc.getLocation().directionTo(bullet.getLocation()).equals(bullet.getDir(), (float) (Math.PI / 2.0));
	}

}
