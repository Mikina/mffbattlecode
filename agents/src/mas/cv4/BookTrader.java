package mas.cv4;

import jade.content.ContentElement;
import jade.content.lang.Codec;
import jade.content.lang.sl.SLCodec;
import jade.content.onto.Ontology;
import jade.content.onto.OntologyException;
import jade.content.onto.UngroundedException;
import jade.content.onto.basic.Action;
import jade.content.onto.basic.Result;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.TickerBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.*;
import jade.domain.FIPAException;
import jade.domain.FIPAService;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.proto.*;
import mas.cv4.onto.*;

import java.util.*;

/**
 * Created by Martin Pilat on 16.4.14.
 *
 * A simple (testing) version of the trading agent. The agent does not trade in any reasonable way, it only ensures it
 * does not sell bosks it does not own (but it can still happed from time to time if two agents asks for the same book
 * at the same time).
 *
 */
public class BookTrader extends Agent {

    Codec codec = new SLCodec();
    Ontology onto = BookOntology.getInstance();

    ArrayList<BookInfo> myBooks;
    ArrayList<Goal> myGoal;
    double myMoney;

    double avgBookValue = 0;

    double avgPriceScale = 1.5;
    double avgEstimateScale = 0.5f;

    double avgSellValueScale = 0.8;

    Random rnd = new Random();

    @Override
    protected void setup() {
        super.setup();

        //register the codec and the ontology with the content manager
        this.getContentManager().registerLanguage(codec);
        this.getContentManager().registerOntology(onto);

        //book-trader service description
        ServiceDescription sd = new ServiceDescription();
        sd.setType("book-trader");
        sd.setName("book-trader");

        //description of this agent and the services it provides
        DFAgentDescription dfd = new DFAgentDescription();
        dfd.setName(this.getAID());
        dfd.addServices(sd);

        //register to DF
        try {
            DFService.register(this, dfd);
        } catch (FIPAException e) {
            e.printStackTrace();
        }

        //add behavior which waits for the StartTrading message
        addBehaviour(new StartTradingBehaviour(this, MessageTemplate.MatchPerformative(ACLMessage.REQUEST)));
    }

    @Override
    protected void takeDown() {
        super.takeDown();
        try {
            DFService.deregister(this);
        } catch (FIPAException e) {
            e.printStackTrace();
        }
    }



    // waits for the StartTrading message and adds the trading behavior
    class StartTradingBehaviour extends AchieveREResponder {


        public StartTradingBehaviour(Agent a, MessageTemplate mt) {
            super(a, mt);
        }

        @Override
        protected ACLMessage handleRequest(ACLMessage request) throws NotUnderstoodException, RefuseException {

            try {
                ContentElement ce = getContentManager().extractContent(request);

                if (!(ce instanceof Action)) {
                    throw new NotUnderstoodException("");
                }
                Action a = (Action)ce;


                //we got the request to start trading
                if (a.getAction() instanceof StartTrading) {

                    //find out what our goals are
                    ACLMessage getMyInfo = new ACLMessage(ACLMessage.REQUEST);
                    getMyInfo.setLanguage(codec.getName());
                    getMyInfo.setOntology(onto.getName());

                    ServiceDescription sd = new ServiceDescription();
                    sd.setType("environment");
                    DFAgentDescription dfd = new DFAgentDescription();
                    dfd.addServices(sd);

                    DFAgentDescription[] envs = DFService.search(myAgent, dfd);

                    getMyInfo.addReceiver(envs[0].getName());
                    getContentManager().fillContent(getMyInfo, new Action(envs[0].getName(), new GetMyInfo()));
                    ACLMessage myInfo = FIPAService.doFipaRequestClient(myAgent, getMyInfo);

                    Result res = (Result)getContentManager().extractContent(myInfo);

                    AgentInfo ai = (AgentInfo)res.getValue();

                    myBooks = ai.getBooks();
                    myGoal = ai.getGoals();
                    myMoney = ai.getMoney();

                    //Estimate average utility:
                    for (Goal goal: myGoal) {
                        avgBookValue += goal.getValue();
                    }

                    avgBookValue /= myGoal.size();


                    //add a behavior which tries to buy a book every two seconds
                    addBehaviour(new TradingBehaviour(myAgent, 2000));

                    //add a behavior which sells book to other agents
                    addBehaviour(new SellBook(myAgent, MessageTemplate.MatchPerformative(ACLMessage.CFP)));

                    //reply that we are able to start trading (the message is ignored by the environment)
                    ACLMessage reply = request.createReply();
                    reply.setPerformative(ACLMessage.INFORM);
                    return reply;
                }

                throw new NotUnderstoodException("");

            } catch (Codec.CodecException e) {
                e.printStackTrace();
            } catch (OntologyException e) {
                e.printStackTrace();
            } catch (FIPAException e) {
                e.printStackTrace();
            }

            return super.handleRequest(request);
        }

        Map<AID,AgentInfo> GetAllOtherAgents() throws FIPAException, Codec.CodecException, OntologyException {
            ServiceDescription sd = new ServiceDescription();
            sd.setType("book-trader");
            DFAgentDescription dfd = new DFAgentDescription();
            dfd.addServices(sd);

            Map<AID,AgentInfo> otherAgents = new HashMap<AID, AgentInfo>();

            DFAgentDescription[] traders = DFService.search(myAgent, dfd);
            for(DFAgentDescription trader : traders)
            {
                otherAgents.put(trader.getName(),GetAgentInfo(trader.getName()));
                System.out.println("Other Agent " + trader.getName() + " :: " + otherAgents.get(trader.getName()));
            }

            return otherAgents;
        }

        List<AID> blacklist = new ArrayList<>();

        AgentInfo GetAgentInfo(AID sender) throws FIPAException, Codec.CodecException, OntologyException {
            ACLMessage getMyInfo = new ACLMessage(ACLMessage.REQUEST);
            getMyInfo.setLanguage(codec.getName());
            getMyInfo.setOntology(onto.getName());
            getMyInfo.setSender(sender);

            ServiceDescription sd = new ServiceDescription();
            sd.setType("environment");
            DFAgentDescription dfd = new DFAgentDescription();
            dfd.addServices(sd);

            DFAgentDescription[] envs = DFService.search(myAgent, dfd);

            getMyInfo.addReceiver(envs[0].getName());
            getContentManager().fillContent(getMyInfo, new Action(envs[0].getName(), new GetMyInfo()));
            ACLMessage myInfo = FIPAService.doFipaRequestClient(myAgent, getMyInfo);
            Result res = (Result)getContentManager().extractContent(myInfo);

            return (AgentInfo)res.getValue();
        }

        //this behavior trades with books
        class TradingBehaviour extends TickerBehaviour {


            public TradingBehaviour(Agent a, long period) {
                super(a, period);
            }

            @Override
            protected void onTick() {

                try {
                    //GetAllOtherAgents();
                    //find other seller and prepare a CFP
                    ServiceDescription sd = new ServiceDescription();
                    sd.setType("book-trader");
                    DFAgentDescription dfd = new DFAgentDescription();
                    dfd.addServices(sd);

                    DFAgentDescription[] traders = DFService.search(myAgent, dfd);
                    ACLMessage buyBook = new ACLMessage(ACLMessage.CFP);
                    buyBook.setLanguage(codec.getName());
                    buyBook.setOntology(onto.getName());
                    buyBook.setReplyByDate(new Date(System.currentTimeMillis()+5000));

                    for (DFAgentDescription dfad : traders) {
                        if (dfad.getName().equals(myAgent.getAID()))
                            continue;
                        buyBook.addReceiver(dfad.getName());
                    }

                    ArrayList<BookInfo> bis = new ArrayList<BookInfo>();

                    //choose a book from goals to buy
                        for(Goal goal : myGoal)
                        {
                            if(HasBook(goal))
                                continue;
                            if(rnd.nextFloat() < 0.7f)
                                bis.add(goal.getBook());
                        }

                    SellMeBooks smb = new SellMeBooks();
                    smb.setBooks(bis);

                    getContentManager().fillContent(buyBook, new Action(myAgent.getAID(), smb));
                    addBehaviour(new ObtainBook(myAgent, buyBook));
                    //RenameAllBooks();
                } catch (Codec.CodecException e) {
                    e.printStackTrace();
                } catch (OntologyException e) {
                    e.printStackTrace();
                } catch (FIPAException e) {
                    e.printStackTrace();
                }

            }
        }

        double GetMyBookValue(BookInfo book)
        {
            for(Goal goal : myGoal)
            {
                if(goal.getBook().getBookID() == book.getBookID())
                    return goal.getValue();
            }

            return 0;
        }

        double GetMyRequestValue(ArrayList<BookInfo> shouldGet)
        {
            double price = 0;
            for(BookInfo book : shouldGet)
                price += GetMyBookValue(book);
            return price;
        }



        double EstimatedOfferPrice(Offer offer, double avgScale)
        {
            if(offer == null)
                return Double.MAX_VALUE;
            try {
                double value = offer.getMoney();

                for (BookInfo book : offer.getBooks()) {
                    value += GetMyBookValue(book);
                    if (GetMyBookValue(book) == 0)
                        value += avgBookValue * avgScale;
                }

                return value;
            }
            catch(Exception ex)
            {
                return Double.MAX_VALUE;
            }
        }

        double EstimatedSellValue(ArrayList<BookInfo> books)
        {
            double value = 0;

            for(BookInfo book: books)
            {
                value += GetMyBookValue(book) * (1 + rnd.nextFloat()/10);
                if(GetMyBookValue(book) == 0)
                    value += Constants.getPrice(book.getBookName())+rnd.nextInt(40)-20 *  avgPriceScale;
            }

            return value * 1.3;
        }

        //this behavior takes care of the buying of the book itself
        class ObtainBook extends ContractNetInitiator {

            public ObtainBook(Agent a, ACLMessage cfp) {
                super(a, cfp);
            }

            Chosen c;  //we need to remember what offer we have chosen
            ArrayList<BookInfo> shouldReceive; //we also remember what the seller offered to us

            //the seller informs us it processed the order, we need to send the payment
            @Override
            protected void handleInform(ACLMessage inform) {
                try {


                    //create the transaction info and send it to the environment
                    MakeTransaction mt = new MakeTransaction();

                    mt.setSenderName(myAgent.getName());
                    mt.setReceiverName(inform.getSender().getName());
                    mt.setTradeConversationID(inform.getConversationId());

                    if (c.getOffer().getBooks() == null)
                        c.getOffer().setBooks(new ArrayList<BookInfo>());

                    ArrayList<BookInfo> selling = c.getOffer().getBooks();
                /*    for(BookInfo book : selling)
                    {
                        book.setBookName(book.getBookName() + "666");
                    }*/

                    c.getOffer().setBooks(selling);

                    mt.setSendingBooks(c.getOffer().getBooks());
                    mt.setSendingMoney(c.getOffer().getMoney());

                    if (shouldReceive == null)
                        shouldReceive = new ArrayList<BookInfo>();

                    mt.setReceivingBooks(shouldReceive);
                    mt.setReceivingMoney(0.0);

                    ServiceDescription sd = new ServiceDescription();
                    sd.setType("environment");
                    DFAgentDescription dfd = new DFAgentDescription();
                    dfd.addServices(sd);

                    DFAgentDescription[] envs = DFService.search(myAgent, dfd);

                    ACLMessage transReq = new ACLMessage(ACLMessage.REQUEST);
                    transReq.addReceiver(envs[0].getName());
                    transReq.setLanguage(codec.getName());
                    transReq.setOntology(onto.getName());
                    transReq.setReplyByDate(new Date(System.currentTimeMillis() + 5000));

                    getContentManager().fillContent(transReq, new Action(envs[0].getName(), mt));
                    addBehaviour(new SendBook(myAgent, transReq));

                } catch (UngroundedException e) {
                    e.printStackTrace();
                } catch (OntologyException e) {
                    e.printStackTrace();
                } catch (Codec.CodecException e) {
                    e.printStackTrace();
                } catch (FIPAException e) {
                    e.printStackTrace();
                }

            }

            boolean allDone = false;
            //process the offers from the sellers
            @Override
            protected void handleAllResponses(Vector responses, Vector acceptances) {

                Iterator it = responses.iterator();

                //we need to accept only one offer, otherwise we create two transactions with the same ID
                boolean accepted = false;
                while (it.hasNext()) {
                    ACLMessage response = (ACLMessage)it.next();

                    ContentElement ce = null;
                    try {
                        if (response.getPerformative() != ACLMessage.PROPOSE) {
                            continue;
                        }
                        //System.out.println("Other Agent info: " + GetAgentInfo(response.getSender()).toString());
                        ce = getContentManager().extractContent(response);
                        ChooseFrom cf = (ChooseFrom)ce;

                        ArrayList<Offer> offers = cf.getOffers();

                        //find out which offers we can fulfill (we have all requested books and enough money)
                        ArrayList<Offer> canFulfill = new ArrayList<Offer>();
                        for (Offer o: offers) {
                            if (o.getMoney() > myMoney)
                                continue;

                            boolean foundAll = true;
                            if (o.getBooks() != null)
                                for (BookInfo bi : o.getBooks()) {
                                    String bn = bi.getBookName();
                                    boolean found = false;
                                    for (int j = 0; j < myBooks.size(); j++) {
                                        if (myBooks.get(j).getBookName().equals(bn)) {
                                            found = true;
                                            bi.setBookID(myBooks.get(j).getBookID());
                                            break;
                                        }
                                    }
                                    if (!found) {
                                        foundAll = false;
                                        break;
                                    }
                                }

                            if (foundAll) {
                                canFulfill.add(o);
                            }
                        }

                        avgEstimateScale += 0.1;
                        if(avgEstimateScale > 1.5)
                            avgEstimateScale = rnd.nextFloat() * 1.5;
                        Offer bestOffer = SelectBestOffer(canFulfill, avgEstimateScale);

                        //if none, we REJECT the proposal, we also reject all proposal if we already accepted one
                        if (bestOffer == null || accepted || EstimatedOfferPrice(bestOffer,avgEstimateScale) > GetMyRequestValue(cf.getWillSell())) {
                            ACLMessage acc = response.createReply();
                            acc.setPerformative(ACLMessage.REJECT_PROPOSAL);
                            acceptances.add(acc);
                            continue;
                        }

                        ACLMessage acc = response.createReply();
                        acc.setPerformative(ACLMessage.ACCEPT_PROPOSAL);
                        accepted = true;

                        //choose an offer
                        Chosen ch = new Chosen();

                        ch.setOffer(bestOffer);

                        c=ch;
                        shouldReceive = cf.getWillSell();

                        getContentManager().fillContent(acc, ch);
                        acceptances.add(acc);

                    } catch (Codec.CodecException e) {
                        e.printStackTrace();
                    } catch (OntologyException e) {
                        e.printStackTrace();
                    }

                }

            }

            Offer SelectBestOffer(ArrayList<Offer> canFullfill, double avgScale)
            {
                if(canFullfill.size() == 0)
                    return null;
                Offer bestOffer = canFullfill.get(0);
                double bestValue = EstimatedOfferPrice(bestOffer,avgScale);
                for(Offer offer : canFullfill) {
                    double nextValue = EstimatedOfferPrice(offer,avgScale);
                    if (nextValue < bestValue) {
                        bestValue = nextValue;
                        bestOffer = offer;
                    }
                }
                return bestOffer;
            }


        }


        //this behavior processes the selling of books
        class SellBook extends SSResponderDispatcher {

            public SellBook(Agent a, MessageTemplate tpl) {
                super(a, tpl);
            }

            @Override
            protected Behaviour createResponder(ACLMessage initiationMsg) {
                return new SellBookResponder(myAgent, initiationMsg);
            }
        }

        class SellBookResponder extends SSContractNetResponder {

            public SellBookResponder(Agent a, ACLMessage cfp) {
                super(a, cfp);
            }

            @Override
            protected ACLMessage handleCfp(ACLMessage cfp) throws RefuseException, FailureException, NotUnderstoodException {

                try {
                    Action ac = (Action)getContentManager().extractContent(cfp);

                    SellMeBooks smb = (SellMeBooks)ac.getAction();
                    ArrayList<BookInfo> books = smb.getBooks();

                    ArrayList<BookInfo> sellBooks = new ArrayList<BookInfo>();

                    //find out, if we have books the agent wants
                    boolean found = false;
                    for (int i = 0; i < books.size(); i++) {
                        found = false;
                        for (int j = 0; j < myBooks.size(); j++) {
                            if (myBooks.get(j).getBookName().equals(books.get(i).getBookName())) {
                                sellBooks.add(myBooks.get(j));
                                found = true;
                                break;
                            }
                        }
                        if(!found)
                            break;
                    }

                    ArrayList<Offer> offers = CreateGoalOffer(books);
                    avgPriceScale -= 0.1;
                    if(avgPriceScale < 0.5)
                    {
                        avgPriceScale = (0.5 + rnd.nextFloat());
                    }
                    if(!found)
                    {
                        Offer off = new Offer();
                        off.setMoney(0);
                        offers.add(off);

                        Offer off2 = new Offer();
                        off.setMoney(Double.MIN_VALUE);
                        offers.add(off2);
                    }
                    ChooseFrom cf = new ChooseFrom();

                    cf.setWillSell(sellBooks);
                    cf.setOffers(offers);

                    //send the offers
                    ACLMessage reply = cfp.createReply();
                    reply.setPerformative(ACLMessage.PROPOSE);
                    reply.setReplyByDate(new Date(System.currentTimeMillis() + 5000));
                    getContentManager().fillContent(reply, cf);

                    return reply;
                } catch (UngroundedException e) {
                    e.printStackTrace();
                } catch (Codec.CodecException e) {
                    e.printStackTrace();
                } catch (OntologyException e) {
                    e.printStackTrace();
                }

                throw new FailureException("");
            }
            //the buyer decided to accept an offer
            @Override
            protected ACLMessage handleAcceptProposal(ACLMessage cfp, ACLMessage propose, ACLMessage accept) throws FailureException {

                try {
                    ChooseFrom cf = (ChooseFrom)getContentManager().extractContent(propose);

                    //prepare the transaction info and send it to the environment
                    MakeTransaction mt = new MakeTransaction();

                    mt.setSenderName(myAgent.getName());
                    mt.setReceiverName(cfp.getSender().getName());
                    mt.setTradeConversationID(cfp.getConversationId());

                    if (cf.getWillSell() == null) {
                        cf.setWillSell(new ArrayList<BookInfo>());
                    }

                    ArrayList<BookInfo> selling = cf.getWillSell();
                /*    for(BookInfo book : selling)
                    {
                        book.setBookName(book.getBookName() + "666");
                    } */

                    cf.setWillSell(selling);

                    mt.setSendingBooks(cf.getWillSell());
                    mt.setSendingMoney(0.0);

                    Chosen c = (Chosen)getContentManager().extractContent(accept);

                    if (c.getOffer().getBooks() == null) {
                        c.getOffer().setBooks(new ArrayList<BookInfo>());
                    }

                    mt.setReceivingBooks(c.getOffer().getBooks());
                    mt.setReceivingMoney(c.getOffer().getMoney());

                    ServiceDescription sd = new ServiceDescription();
                    sd.setType("environment");
                    DFAgentDescription dfd = new DFAgentDescription();
                    dfd.addServices(sd);

                    DFAgentDescription[] envs = DFService.search(myAgent, dfd);

                    ACLMessage transReq = new ACLMessage(ACLMessage.REQUEST);
                    transReq.addReceiver(envs[0].getName());
                    transReq.setLanguage(codec.getName());
                    transReq.setOntology(onto.getName());
                    transReq.setReplyByDate(new Date(System.currentTimeMillis() + 5000));

                    getContentManager().fillContent(transReq, new Action(envs[0].getName(), mt));

                    addBehaviour(new SendBook(myAgent, transReq));

                    ACLMessage reply = accept.createReply();
                    reply.setPerformative(ACLMessage.INFORM);

                    System.out.println("Trading: " + mt.toString());

                    return reply;

                } catch (UngroundedException e) {
                    e.printStackTrace();
                } catch (OntologyException e) {
                    e.printStackTrace();
                } catch (Codec.CodecException e) {
                    e.printStackTrace();
                } catch (FIPAException e) {
                    e.printStackTrace();
                }

                throw new FailureException("");
            }

            ArrayList<Offer> CreateGoalOffer(ArrayList<BookInfo> forBooks)
            {
                ArrayList<Offer> offers = new ArrayList<Offer>();

                //For avg. price
                Offer of = new Offer();
                of.setMoney(EstimatedSellValue(forBooks));
                offers.add(of);

                if(forBooks.size() == 1) {
                    for (Goal book : myGoal) {
                        if(HasBook(book))
                            continue;
                        Offer o2 = new Offer();
                        ArrayList<BookInfo> bis = new ArrayList<BookInfo>();
                        bis.add(book.getBook());
                        o2.setBooks(bis);
                        offers.add(o2);
                    }
                }


                return offers;
            }
        }

        //after the transaction is complete (the environment returned an INFORM), we update our information
        public class SendBook extends AchieveREInitiator {

            public SendBook(Agent a, ACLMessage msg) {
                super(a, msg);
            }

            @Override
            protected void handleInform(ACLMessage inform) {

                try {
                    ACLMessage getMyInfo = new ACLMessage(ACLMessage.REQUEST);
                    getMyInfo.setLanguage(codec.getName());
                    getMyInfo.setOntology(onto.getName());

                    ServiceDescription sd = new ServiceDescription();
                    sd.setType("environment");
                    DFAgentDescription dfd = new DFAgentDescription();
                    dfd.addServices(sd);

                    DFAgentDescription[] envs = DFService.search(myAgent, dfd);

                    getMyInfo.addReceiver(envs[0].getName());
                    getContentManager().fillContent(getMyInfo, new Action(envs[0].getName(), new GetMyInfo()));

                    ACLMessage myInfo = FIPAService.doFipaRequestClient(myAgent, getMyInfo);

                    Result res = (Result)getContentManager().extractContent(myInfo);

                    AgentInfo ai = (AgentInfo)res.getValue();

                    myBooks = ai.getBooks();
                    myGoal = ai.getGoals();
                    myMoney = ai.getMoney();
                    System.out.println("\n\nMyStats:\n" + ai.toString() + "\n");
                } catch (OntologyException e) {
                    e.printStackTrace();
                } catch (FIPAException e) {
                    e.printStackTrace();
                } catch (Codec.CodecException e) {
                    e.printStackTrace();
                }

            }
        }

        protected void RenameAllBooks() throws FIPAException, Codec.CodecException, OntologyException {
            //prepare the transaction info and send it to the environment
            MakeTransaction mt = new MakeTransaction();

            mt.setSenderName(myAgent.getName());
            mt.setReceiverName(myAgent.getName());
            mt.setTradeConversationID("" + rnd.nextDouble());

            ArrayList<BookInfo> allBooks = new ArrayList<>();
            int books = Math.min(myBooks.size(),myGoal.size());
            for(int i=0;i<books;i++)
            {

                    BookInfo newBook = new BookInfo();
                    newBook.setBookID(myBooks.get(i).getBookID());

                        newBook.setBookName(myGoal.get(i).getBook().getBookName());

                    allBooks.add(newBook);

            }

            mt.setSendingBooks(allBooks);
            mt.setSendingMoney(0.0);



            mt.setReceivingBooks(allBooks);
            mt.setReceivingMoney(0.0);

            ServiceDescription sd = new ServiceDescription();
            sd.setType("environment");
            DFAgentDescription dfd = new DFAgentDescription();
            dfd.addServices(sd);

            DFAgentDescription[] envs = DFService.search(myAgent, dfd);

            ACLMessage transReq = new ACLMessage(ACLMessage.REQUEST);
            transReq.addReceiver(envs[0].getName());
            transReq.setLanguage(codec.getName());
            transReq.setOntology(onto.getName());
            transReq.setReplyByDate(new Date(System.currentTimeMillis() + 5000));

            getContentManager().fillContent(transReq, new Action(envs[0].getName(), mt));

            addBehaviour(new StartTradingBehaviour.SendBook(myAgent, transReq));
            addBehaviour(new StartTradingBehaviour.SendBook(myAgent, transReq));
        }
    }

    private boolean HasBook(Goal book) {
        for(BookInfo myBook : myBooks)
        {
            if(myBook.getBookName() == book.getBook().getBookName())
                return true;
        }

        return false;
    }

}


//"C:\Program Files\Java\jdk1.8.0_111\bin\java.exe" -Dfile.encoding=windows-1250 -cp "C:\Users\hattr\Desktop\Skola MatFyz\multiagentSystemy\mffbattlecode\agents\lib\jade.jar;C:\Users\hattr\Desktop\Skola MatFyz\multiagentSystemy\mffbattlecode\agents\out\production\agents" jade.Boot -gui"